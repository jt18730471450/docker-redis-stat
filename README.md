# redis-stat in docker


Based on [redis-stat](https://github.com/junegunn/redis-stat)


```sh
docker run -d -p 8080:8080 redis-stat [--auth=PASSWORD] [HOST[:PORT] ...] [INTERVAL [COUNT]]
```