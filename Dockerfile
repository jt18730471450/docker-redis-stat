FROM ruby:latest

MAINTAINER Zonesan Chai <chaizs@asiainfo.com>

RUN gem install redis-stat

EXPOSE 8080

ENTRYPOINT [ "redis-stat","--server=8080" ]
